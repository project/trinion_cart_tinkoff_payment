<?php

namespace Drupal\trinion_cart_tinkoff_payment\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Trinion Tinkoff payment settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trinion_cart_tinkoff_payment_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['trinion_cart_tinkoff_payment.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['terminal_key'] = [
      '#type' => 'textfield',
      '#title' => 'Terminal key',
      '#default_value' => $this->config('trinion_cart_tinkoff_payment.settings')->get('terminal_key'),
    ];
    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => 'Secret key',
      '#default_value' => $this->config('trinion_cart_tinkoff_payment.settings')->get('secret_key'),
    ];
    $form['payment_method'] = [
      '#type' => 'select',
      '#title' => 'Payment method',
      '#options' => [
        'full_prepayment' => 'Предоплата 100%',
        'prepayment'      => 'Предоплата',
        'advance'         => 'Аванc',
        'full_payment'    => 'Полный расчет',
        'partial_payment' => t('Partial settlement and credit'),
        'credit'          => t('Transfer on credit'),
        'credit_payment'  => t('Loan payment'),
      ],
      '#default_value' => $this->config('trinion_cart_tinkoff_payment.settings')->get('payment_method'),
    ];
    $form['payment_object'] = [
      '#type' => 'select',
      '#title' => 'Payment object',
      '#options' => [
        'commodity'             => 'Товар',
        'excise'                => 'Подакцизный товар',
        'job'                   => 'Работа',
        'service'               => 'Услуга',
        'gambling_bet'          => 'Ставка азартной игры',
        'gambling_prize'        => 'Выигрыш азартной игры',
        'lottery'               => 'Лотерейный билет',
        'lottery_prize'         => 'Выигрыш лотереи',
        'intellectual_activity' => 'Предоставление результатов интеллектуальной деятельности',
        'payment'               => 'Платеж',
        'agent_commission'      => 'Агентское вознаграждение',
        'composite'             => 'Составной предмет расчета',
        'another'               => 'Иной предмет расчета',
      ],
      '#default_value' => $this->config('trinion_cart_tinkoff_payment.settings')->get('payment_object'),
    ];
    $form['tax'] = [
      '#type' => 'select',
      '#title' => 'Tax',
      '#options' => [
        'none'  => 'Без НДС',
        'vat0'  => 'НДС 0%',
        'vat10' => 'НДС 10%',
        'vat20' => 'НДС 20%'
      ],
      '#default_value' => $this->config('trinion_cart_tinkoff_payment.settings')->get('tax'),
    ];
    $form['taxation'] = [
      '#type' => 'select',
      '#title' => 'Tax',
      '#options' => [
        'osn'                => 'Общая СН',
        'usn_income'         => 'Упрощенная СН (доходы)',
        'usn_income_outcome' => 'Упрощенная СН (доходы минус расходы)',
        'envd'               => 'Единый налог на вмененный доход',
        'esn'                => 'Единый сельскохозяйственный налог',
        'patent'             => 'Патентная СН'
      ],
      '#default_value' => $this->config('trinion_cart_tinkoff_payment.settings')->get('taxation'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('trinion_cart_tinkoff_payment.settings')
      ->set('terminal_key', $form_state->getValue('terminal_key'))
      ->set('secret_key', $form_state->getValue('secret_key'))
      ->set('payment_method', $form_state->getValue('payment_method'))
      ->set('payment_object', $form_state->getValue('payment_object'))
      ->set('tax', $form_state->getValue('tax'))
      ->set('taxation', $form_state->getValue('taxation'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
