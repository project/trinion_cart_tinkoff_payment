<?php

namespace Drupal\trinion_cart_tinkoff_payment\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\trinion_cart\Event\PaymentEvent;

class TinkoffController extends ControllerBase
{
    public function complete()
    {
        $json = file_get_contents("php://input");
        \Drupal::logger('payment')->debug($json);
        $flow = json_decode($json);
        $flow->Success = $flow->Success ? 'true' : 'false';
        foreach ($flow as $key => $item) {
            $request[$key] = $item;
        }
        $orderId = intval($request['OrderId']);
        $order = Node::load($orderId);

        if(!$order){
            exit('NOTOK');
        }

        if ($order == FALSE) {
            exit('NOTOK');
        }
        if ($this->get_tinkoff_token($request) != $request['Token']) {
            exit('NOTOK');
        }

        if ($request['Status'] == 'AUTHORIZED' && $order->order_status == 'payment_received') exit('OK');
        switch ($request['Status']) {
            case 'CONFIRMED':
                $order = \Drupal::service('trinion_cart.payment')->processPayment($order, $flow->Amount / 100, 'TinkoffPayment');

                $dispatcher = \Drupal::service('event_dispatcher');
                $event = new PaymentEvent($order);
                $dispatcher->dispatch($event, \Drupal\trinion_cart\Event\PaymentEvent::PAYMENT_SUCCESS);
                break;
            case 'AUTHORIZED':
            case 'CANCELED':
            case 'REJECTED':
            case 'REVERSED':
            case 'REFUNDED':
        }
        exit('OK');
    }

    public function success()
    {
        $output = array();
        $output['#markup'] = t('Thank you for order!');
        return $output;
    }

    public function error()
    {
        $output = array();
        $output['#markup'] = t('Payment error!');
        return $output;
    }

    public function get_tinkoff_token($request)
    {
        $request['Password'] = \Drupal::config('trinion_cart_tinkoff_payment.settings')->get('secret_key');
        ksort($request);
        unset($request['Token']);
        $values = implode('', array_values($request));
        return hash('sha256', $values);
    }
}
